#include "efi/types.h"
#include "efi/system-table.h"

const efi_system_table      *kSystemTable;
const efi_boot_services     *kBootServices;
const efi_runtime_services  *kRuntimeServices;
efi_handle kImage;

extern "C" efi_status
efi_main(efi_handle image, efi_system_table *systemTable)
{
    kImage = image;
    kSystemTable = systemTable;
    kBootServices = systemTable->BootServices;
    kRuntimeServices = systemTable->RuntimeServices;

    kSystemTable->ConOut->OutputString(kSystemTable->ConOut, (char16_t*)L"Hello world!");

	return EFI_SUCCESS;
}
