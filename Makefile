STATIC ?=

default:
	${CC} elf2efi.c -I include/ -DEFI_TARGET32=1 -o elf2efi32 $(STATIC)
	${CC} elf2efi.c -I include/ -DEFI_TARGET64=1 -o elf2efi64 $(STATIC)
clean:
	rm -f elf2efi32 elf2efi64
